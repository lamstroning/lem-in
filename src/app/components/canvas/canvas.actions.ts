import { createAction, props } from '@ngrx/store';

export const updateCanvasContext = createAction(
  '[Canvas Component] Update Canvas context',
  props<{ canvas: HTMLCanvasElement }>()
);

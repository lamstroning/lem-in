import {
  AfterContentInit,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild
} from '@angular/core';
import {Store} from "@ngrx/store";
import {updateCanvasContext} from "./canvas.actions";

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements AfterContentInit{
  @ViewChild('canvas', { static: true })

  canvasRef: ElementRef<HTMLCanvasElement>
  private canvas: HTMLCanvasElement
  private context: CanvasRenderingContext2D | null

  private isDrawing = false
  private lastX: number
  private lastY: number

  constructor(private store: Store) {}

  ngAfterContentInit() {
    this.canvas = this.canvasRef.nativeElement
    this.context = this.canvas?.getContext('2d')

    if (this.context) {
      this.store.dispatch(updateCanvasContext({canvas: this.canvasRef.nativeElement}))
      this.resize()
    }
  }

  onMouseDown(event: MouseEvent) {
    this.isDrawing = true
    this.lastX = event.offsetX
    this.lastY = event.offsetY
  }

  resize() {
    this.canvasRef.nativeElement.height = window.innerHeight
    this.canvasRef.nativeElement.width = window.innerWidth - 200
  }

  onMouseUp(event: MouseEvent) {
    this.isDrawing = false
  }

  onMouseMove(event: MouseEvent) {
    if (!this.isDrawing || !this.context) {
      return
    }
    this.context.beginPath()
    this.context.moveTo(this.lastX, this.lastY)
    this.context.lineTo(event.offsetX, event.offsetY)
    this.context.stroke()
    this.lastX = event.offsetX
    this.lastY = event.offsetY
  }
  /*ngAfterContentInit() {
    this.canvas = this.canvasRef.nativeElement
    this.context = this.canvas.getContext('2d') || {} as CanvasRenderingContext2D
    // this.store.dispatch(setCanvas())

    this.getContext.emit(this.context)
    this.resize()

    this.drew()
  }*/
  /*
    ngAfterContentChecked() {
      this.drew()
    }

    drew = () => console.log()

    onClick = (event: Event) => {console.log(event)}

    resize() {
      this.canvas.height = window.innerHeight
      this.canvas.width = window.innerWidth - 200
    }*/
}

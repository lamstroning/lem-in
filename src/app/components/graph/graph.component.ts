import {
  AfterContentChecked,
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core'

import {
  COLUMNS_COUNT,
  connectNodes,
  findNode,
  getNewNode,
} from '../../../utils/utils'

import {TNode} from './node-model'
import {Store} from '@ngrx/store'
import {AppState} from '../../../store/app.store'
import {Subscription} from 'rxjs'

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphComponent implements AfterContentInit, AfterContentChecked, OnInit, OnDestroy {
  subscription: Subscription;

  private editMode = false
  private context?: CanvasRenderingContext2D
  private canvas: HTMLCanvasElement
  public nodes: TNode[]

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.subscription = this.store.select(state => state.canvasReducer)
      .subscribe(data => {
        this.context = data.context;
        this.canvas = data.canvas || {} as HTMLCanvasElement;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngAfterContentInit() {
    this.nodes = this.generateNodes(COLUMNS_COUNT)
    this.store.select('canvasReducer').subscribe(({context}) => {
      this.context = context
      console.log(context)
    })
  }

  ngAfterContentChecked() {
    for (const node of this.nodes)
      this.drewNode(node)
  }

  drewNode = (node: TNode) => {
    const context = this.context
    const {canvasContext: {x, y, radius, fillStyle, strokeStyle}} = node

    if (!context)
      return

    context.beginPath()
    context.fillStyle = fillStyle
    context.arc(x, y, radius, 0, Math.PI * 2, true)
    context.strokeStyle = strokeStyle
    context.stroke()
    context.fill()
    context.font = '12px serif';
    context.fillStyle = 'black';
    context.textAlign = 'center'

    const nodeLabel = (node.id === 0 && 'start') || (node.isEnd && 'end') || String(node.id)

    context.fillText(nodeLabel, x, y - 20);

    this.drewEdges(node)
  }

  drewEdges = (node: TNode) => {
    const {canvasContext} = node
    const context = this.context

    if (!context)
      return

    for (const nextNode of node.siblings) {
      context.moveTo(canvasContext.x,canvasContext.y)
      context.lineTo(nextNode.canvasContext.x, nextNode.canvasContext.y)
      context.strokeStyle = '#000'
      context.lineWidth = 1
      context.stroke()
    }
  }

  /*generateNodes = (nodesCount: number): TNode[][] => {
    const graph: TNode[][] = []
    let rows = 0
    let cells = 0
    let id = 0

    while(rows++ < nodesCount) {
      const nextNodesCount = id === 0 ? 1 : getRandomNumber(this.minRows, this.maxRows)
      const children: TNode[] = []

      while(cells++ < nextNodesCount) {
        const newNode = getNewNode(id++, rows, nextNodesCount, cells)
        children.push(newNode)
      }

      cells = 0
      graph.push(children)
    }
    graph.push([getNewNode(id++, rows, 1, 1, true)])

    return connectNodes(graph)
  }
*/
  generateNodes = (nodesCount: number): TNode[] => {
    const graph: TNode[] = []
    let id = 0

    while(id < nodesCount) {
      graph.push(getNewNode(id++, this.canvas))
      /*const nextNodesCount = id === 0 ? 1 : getRandomNumber(this.minRows, this.maxRows)
      const children: TNode[] = []

      while(cells++ < nextNodesCount) {
        const newNode = getNewNode(id++, rows, nextNodesCount, cells)
        children.push(newNode)
      }

      cells = 0
      graph.push(children)*/
    }

    return connectNodes(graph)
  }

  onClick = (event: Event) => {
    if (this.editMode) {
      console.log(event)
    }
  }

  searchWay(id: number, weight = 1): number {
    const node = findNode(id, this.nodes)
    let minWeight = weight

    if (!node)
      return 0

    for (const nextNode of node.siblings) {
      if (nextNode.isEnd)
        return weight + 1

      if (nextNode.opened && (nextNode.weight == 0 || nextNode.weight <= weight)) {
        nextNode.opened = false
        nextNode.weight = weight

        const nextWeight = this.searchWay(nextNode.id, weight + 1)
        minWeight = nextWeight < minWeight ? nextWeight : minWeight
      }
    }

    return 0
  }

  changeEditMode = () => this.editMode = !this.editMode
}

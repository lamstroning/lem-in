export type TCanvasContext = {
  x: number,
  y: number,
  radius: number,
  fillStyle: string,
  strokeStyle: string,
}

export type TNode = {
  id: number,
  step: number,
  opened: boolean,
  isEnd: boolean,
  siblings: TNode[],
  canvasContext: TCanvasContext
  weight: number,
}

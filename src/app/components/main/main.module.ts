import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { MainComponent } from './main.component'
import { GraphComponent } from '../sections/graph/graph.component'
import { MatrixComponent } from '../sections/matrix/matrix.component'
import { CanvasComponent } from '../common/canvas/canvas.component'

import { StoreModule } from '@ngrx/store'
import { canvasReducer } from '../../store/canvas.reducer'

@NgModule({
  declarations: [
    MainComponent,
    GraphComponent,
    MatrixComponent,
    CanvasComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({ canvasReducer })
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class MainModule { }

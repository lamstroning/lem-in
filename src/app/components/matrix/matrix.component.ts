import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AppState} from "../../../store/app.store";
import {Store} from "@ngrx/store";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-matrix',
  templateUrl: './matrix.component.html',
  styleUrls: ['./matrix.component.css']
})
export class MatrixComponent implements AfterViewInit, OnInit {
  context?: CanvasRenderingContext2D
  subscription: Subscription

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.subscription = this.store.select(state => state.canvasReducer.context)
      .subscribe(data => {
        this.context = data
      });
  }
  ngAfterViewInit() {
    const ctx = this.context
    if (!ctx)
      return
    const cellSize = 50
    const numRows = 3
    const numCols = 3
    const margin = 10

    for (let row = 0; row < numRows; row++) {
      for (let col = 0; col < numCols; col++) {
        const x = col * cellSize + margin
        const y = row * cellSize + margin
        const width = cellSize - margin * 2
        const height = cellSize - margin * 2

        ctx.fillStyle = '#fff'
        ctx.fillRect(x, y, width, height)

        ctx.strokeStyle = '#000'
        ctx.strokeRect(x, y, width, height)
      }
    }
  }
}

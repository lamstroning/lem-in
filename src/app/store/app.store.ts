export const DIAMETER = 32
export const RADIUS = DIAMETER / 2
export const EDGES = 100

export const NODES_COUNT = 100

export type TGraphReducer = {
  radius: number
  diameter: number
  edges: number
  nodesCount: number
}

export type TCanvasReducer = {
  canvas?: HTMLCanvasElement
  context?: CanvasRenderingContext2D
}

export interface AppState {
  canvasReducer: TCanvasReducer
  graphReducer: TGraphReducer
}

export const initialState: AppState = {
  canvasReducer: {},
  graphReducer: {
    radius: DIAMETER,
    diameter: RADIUS,
    edges: EDGES,
    nodesCount: NODES_COUNT
  }
}

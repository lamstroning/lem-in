import { createReducer, on } from '@ngrx/store';
import {initialState} from "./app.store";
import {updateCanvasContext} from "../components/canvas/canvas.actions";

export const canvasReducer = createReducer(
  initialState,
  on(updateCanvasContext, (state, { canvas, context }) => ({
      ...state,
      canvas,
      context
    })
  )
)

import {TNode} from "../components/sections/graph/node-model";

export const RADIUS = 32 / 2
export const WEIGHT = RADIUS * 2
export const PADDING = 16
export const SIZE = WEIGHT + PADDING
export const EDGES = 1

export const COLUMNS_COUNT = 3

export const MIN_ROWS = 1
export const MAX_ROWS = 5

export const CONTEXT_PROPS = {
  radius: RADIUS,
  fillStyle: '#fff',
  strokeStyle: '#000',
}

const getX = (rows: number) => SIZE * rows + getRandomNumber(-RADIUS, RADIUS)
const getY = (nodesCount: number, currentCell: number) => (window.innerHeight / (nodesCount + 1)) * currentCell + getRandomNumber(-RADIUS, RADIUS)

export const getNewNode = (
  id: number,
  step: number,
  nextNodesCount: number,
  cells: number,
  isEnd: boolean = false
): TNode => {
  return ({
    id,
    step,
    isEnd,
    siblings: [],
    opened: id !== 0,
    weight: 0,
    canvasContext: {
      x: getX(step),
      y: getY(nextNodesCount, cells),
      ...CONTEXT_PROPS
    }
  })
}

export const getFlatList = (nodeList: TNode[][]) => nodeList.flatMap(node => node)

export const findNode = (id: number, nodeList: TNode[][]) =>
  getFlatList(nodeList).find((node) => node.id == id)

export const findSiblings = (id: number, node: TNode) =>
  node.siblings.find((sibling: TNode) => sibling.id == id)

export const getRandomNumber = (min: number, max: number) =>
  Math.floor(Math.random() * (Math.ceil(max) - Math.ceil(min) + 1) + Math.ceil(min))


export const connectNodes = (nodeList: TNode[][]) => {
  const flatNodeList = getFlatList(nodeList)
  const flatNodeListLength = flatNodeList.length

  for (const nodes of nodeList) {
    for (const node of nodes) {
      const isDrew = Boolean(getRandomNumber(0, EDGES)) || true


      if (isDrew) {
        const id = getRandomNumber(0, flatNodeListLength)
        const nextNode = findNode(id, nodeList)

        if (nextNode && id !== node.id && !findSiblings(id, node)) {
          node.siblings.push(nextNode)

          if (!findSiblings(id, nextNode))
            nextNode.siblings.push(node)
        }
      }
    }
  }

  return nodeList
}
